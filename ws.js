const ws = require('ws');
const net = require('net');

const wss = (server) => {
  const ipcSocketClient = net.createConnection("/tmp/guirlande");
  console.log('WebSocket initialized')
  
  ipcSocketClient.on("connect", () => {
    console.log("connected to /tmp/guirlande");
  })
  /*ipcSocketClient.on("data", (data) => {
    console.log(data);
  })*/
  ipcSocketClient.on("error", (err) => {
    console.log(err)
  })

  const wsServer = new ws.Server({server});
  wsServer.on('connection', (ws) => {
	  ws.on('message', (message) => {
      let decodedMsg = JSON.parse(message);
      switch (decodedMsg.type) {
        case 'status':
          //console.log(`received: ${message}`);
          ws.send(`Hello, you sent ${message}`);
          break;
        case 'data':
        case 'mode':
          //console.log(`received: ${message}`);
          ipcSocketClient.write(message);
          break;
        default:
          let errorMsg = `Error: can't handle your message ${message}`;
          console.warn(errorMsg)
          ws.send(errorMsg);
      }
      
	  });
	  ws.send(`Connection established with rancy-socket`);
  });
  return wsServer
}

module.exports = wss;
